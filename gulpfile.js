'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const gulpPug = require('gulp-pug');
const runSequence = require('run-sequence');
const rimraf = require('rimraf');
const browserSync = require('browser-sync');
const watch = require('gulp-watch');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const data = require('gulp-data');
const path = require('path');
const plumber = require('gulp-plumber');
const svgSprite = require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');
const postcss = require('gulp-postcss');
const uncss = require('gulp-uncss');
const styleInject = require('gulp-style-inject');
const _ = require('lodash');
const pug = require('pug');



const paths = {
  src: {
    style: './src/*/style/**/*.scss',
    pug: './src/**/*.pug',
    i: './src/*/i/*.*'
  },
  dest: {
    common: './build/',
    i: './build/'
  }
};

const servConfig = {
  server: {
    baseDir: 'build/'
  },
  open: false,
  port: 9000,
  reloadDelay: 300
};

gulp.task('style', () => {
  const processors = [
    require('autoprefixer'),
    require('postcss-fixes'),
    // require('doiuse')({
    // browsers: [
    //   '> 1%',
    //   'last 15 Chrome versions',
    //   'last 15 Opera versions',
    //   'IE >= 11',
    //   'last 17 Firefox versions',
    //   'Safari >= 9',
    //   'Android >= 4',
    //   'UCAndroid >= 10',
    //   'iOS >= 8'
    // ],
    // ignore: ['outline']
    // }),
    require('postcss-sorting'),
    require('cssnano')
  ];
  return gulp.src(paths.src.style)
    .pipe(sass().on('error', sass.logError))
    // .pipe(uncss({
    //   html: ['./build/travel/index.html'],
    //   ignore: [/^is/, /^@/],
    //   report: true
    // }))
    .pipe(postcss(processors) )
    .pipe(gulp.dest(paths.dest.common))
    .pipe(browserSync.stream());
});


gulp.task('style:prod', () => {
  const processors = [
    require('autoprefixer'),
    require('postcss-fixes'),
    // require('doiuse')({
    // browsers: [
    //   '> 1%',
    //   'last 15 Chrome versions',
    //   'last 15 Opera versions',
    //   'IE >= 11',
    //   'last 17 Firefox versions',
    //   'Safari >= 9',
    //   'Android >= 4',
    //   'UCAndroid >= 10',
    //   'iOS >= 8'
    // ],
    // ignore: ['outline']
    // }),
    require('cssnano')
  ];
  return gulp.src(paths.src.style)
    // .pipe(replace('$prod: false;', '$prod: "true";'))
    .pipe(sass().on('error', sass.logError))
    .pipe(uncss({
      html: ['./build/**/*.html'],
      ignore: [/^rtl/, /^is/, /^@/],
      report: true
    }))
    .pipe(postcss(processors) )
    .pipe(gulp.dest(paths.dest.common))
    .pipe(browserSync.stream());
});

gulp.task('pug', () => {
  return gulp.src([paths.src.pug, '!src/*/_*.pug'])
    .pipe(plumber())
    .pipe(data((file) => {
      let dir = path.dirname(file.path);
      const getCurrentDir = (patth) => {
        return patth.split('\\').pop();
      };
      let currentDir = getCurrentDir(dir);
      const locale = require(`./src/${currentDir}/locale.json`);
      return {locale};
    }))
   .pipe(gulpPug())
   .pipe(gulp.dest(paths.dest.common))
   .pipe(browserSync.stream());
});

gulp.task('inject-styles', () => {
  return gulp.src(['build/**/*.html'])
  .pipe(styleInject())
  .pipe(gulp.dest(paths.dest.common));
});

gulp.task('img:sync', () => {
  return gulp.src([paths.src.i])
    .pipe(imagemin({
        progressive: true,
        use: [pngquant()],
        interlaced: true
    }))
    .pipe(gulp.dest(paths.dest.common))
    .pipe(browserSync.stream());
});

// gulp.task('svgSprite', () => {
//   return gulp.src('./src/*/i/svg-sprite/*.svg')
//     .pipe(svgmin({
//       js2svg: {
//         pretty: true
//       }
//     }))
//     .pipe(cheerio({
//       run: ($) => {
//         $('[fill]').removeAttr('fill');
//         $('[stroke]').removeAttr('stroke');
//         $('[style]').removeAttr('style');
//       },
//       parserOptions: {xmlMode: true}
//     }))
//     .pipe(replace('&gt;', '>'))
//     .pipe(svgSprite({
//       shape: {
//         id: {
//           generator: (name) => {
//             return path.basename(name).replace('.svg', '');
//           }
//         },
//       },
//       mode: {
//         symbol: {
//           sprite: '../build/travel/i/sprite.svg',
//           render: {
//             scss: {
//               dest: '../src/travel/style/_svg-sprite.scss',
//               template: 'src/common/style/_svg-sprite-template.scss'
//             }
//           }
//         }
//       }
//     }))
//     .pipe(gulp.dest('./'));
// });

gulp.task('webserver', () => {
  browserSync(servConfig);
});
gulp.task('watch', () => {
  watch([paths.src.pug], () => {
    gulp.start('pug');
  });
  watch([paths.src.style], () => {
    gulp.start('style');
  });
});

gulp.task('clean', (cb) => {
  rimraf('./build/', cb);
});



function requireUncached($module) {
  delete require.cache[require.resolve($module)];
  return require($module);
}

function getFileFolder(dir) {
  return dir.split('\\').slice(0, -1).pop()
}

function getFileDir(dir) {
  return dir.split('\\').slice(0, -1).join('\\')
}

const foreach = require('gulp-foreach');
const through2 = require('through2').obj;
const fs = require('fs');
const Vinyl = require('vinyl');

gulp.task('pug:prod', () => {

  return gulp.src([paths.src.pug, '!src/*/_*.pug'])
    // .pipe(replace('const PROD = false;', 'const PROD = true;'))
    .pipe(through2(function(file, enc, cb) {

        let fileDir = getFileFolder(file.path);

        let data = requireUncached(`${getFileDir(file.path)}\\locale.json`);

        _.forIn(data, (val, key) => {

          let fileHtml = pug.renderFile(file.path,
            {
              locale: val
            })

          let fileNew = new Vinyl({
            path: `${getFileFolder(file.path)}\\${key}.html`,
            contents: new Buffer(fileHtml)
          })
          this.push(fileNew);
        })

        cb();
    }))

    .pipe(gulp.dest(paths.dest.common))
    ;
});




gulp.task('default', () => {
  runSequence('clean', 'pug', 'style', 'img:sync',
    'webserver', 'watch');
});

gulp.task('prod', () => {
  runSequence('clean', 'pug:prod', 'style:prod', 'img:sync', 'inject-styles');
});
